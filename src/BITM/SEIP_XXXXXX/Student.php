<?php
/**
 * Created by PhpStorm.
 * User: Trainer 402
 * Date: 1/22/2017
 * Time: 7:23 PM
 */

namespace App;


class Student
{
    private $name;
    private $studentID;

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setStudentID($studentID)
    {
        $this->studentID = $studentID;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getStudentID()
    {
        return $this->studentID;
    }
}